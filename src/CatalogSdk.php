<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/catalog_sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\Dexes\catalog_sdk;

/**
 * Class CatalogSDK.
 *
 * Simple class holding various constants used by the catalog_sdk Drupal module.
 */
class CatalogSdk
{
  public const SETTINGS_KEY = 'catalog_sdk.settings';

  public const SETTINGS_FORM_ID = 'catalog_sdk_configure_catalog_api_form';

  public const LOGGER_CHANNEL = 'catalog_sdk';
}
