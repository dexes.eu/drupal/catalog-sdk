<?php

/**
 * This file is part of the dexes-drupal/catalog_sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\Dexes\catalog_sdk\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Dexes\catalog_sdk\CatalogSdk;

/**
 * Class ConfigureCatalogApiForm.
 *
 * Form to configure the settings to connect a Drupal instance to the Catalog
 * API.
 */
class ConfigureCatalogApiForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return CatalogSdk::SETTINGS_FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    if ($this->isManaged(CatalogSdk::SETTINGS_KEY)) {
      $this->messenger()->addError(
        $this->t('The Catalog API settings are managed by your hosting provider. Contact an administrator to have them changed.')
      );

      return $form;
    }

    $config = $this->config(CatalogSdk::SETTINGS_KEY);

    $form['api_settings'] = [
      '#type'        => 'details',
      '#title'       => $this->t('API Settings'),
      '#description' => $this->t('The settings for connecting to the Catalog API.'),
      '#open'        => TRUE,
    ];

    $form['api_settings']['api_endpoint'] = [
      '#type'          => 'url',
      '#title'         => $this->t('API endpoint'),
      '#default_value' => $config->get('api_endpoint'),
      '#placeholder'   => 'https://',
    ];

    $form['api_settings']['api_key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API key'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['api_settings']['client_user_actions_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#description'   => $this->t('When enabled user that are created automatically get added to the Catalog API.'),
      '#default_value' => $config->get('client_user_actions.enabled') ?? FALSE,
      '#required'      => FALSE,
    ];

    $form['api_settings']['client_user_actions_username_prefix'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Username prefix'),
      '#description'   => $this->t('The prefix is used in the Catalog API so usernames across multiple dataspaces don\'t collide.'),
      '#default_value' => $config->get('client_user_actions.username_prefix') ?? 'drupal_user_',
      '#required'      => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(CatalogSdk::SETTINGS_KEY)) {
      return;
    }

    $config = $this->config(CatalogSdk::SETTINGS_KEY);
    $config->set('api_endpoint', $form_state->getValue('api_endpoint'));
    $config->set('api_key', $form_state->getValue('api_key'));
    $config->set('client_user_actions.enabled', $form_state->getValue('client_user_actions_enabled') === 1);
    $config->set('client_user_actions.username_prefix', $form_state->getValue('client_user_actions_username_prefix'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [CatalogSDK::SETTINGS_KEY];
  }

  /**
   * Determine if the settings by the given key are managed via the
   * `settings.php` file.
   *
   * @return bool Whether the settings are managed
   */
  private function isManaged(string $settingsName): bool
  {
    global $config;

    return !empty($config[$settingsName]['is_managed']);
  }
}
