<?php

/**
 * This file is part of the dexes-drupal/catalog_sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\Dexes\catalog_sdk;

use Dexes\CatalogSdk\HttpRequestService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use GuzzleHttp\Psr7\HttpFactory;

/**
 * Class HttpRequestServiceFactory.
 *
 * Factory class for creating HttpRequestService implementations.
 */
class HttpRequestServiceFactory
{
  /**
   * Creates a `RequestService` for the current user.
   *
   * @param ClientFactory          $clientFactory The GuzzleHttp\ClientInterface factory
   * @param ConfigFactoryInterface $configFactory The factory for retrieving module settings
   *
   * @return HttpRequestService The created instance
   */
  public static function createForUser(ClientFactory $clientFactory,
                                       ConfigFactoryInterface $configFactory,
                                       AccountInterface $user): HttpRequestService
  {
    $loadedUser     = User::load($user->id());
    $requestService = self::create($clientFactory, $configFactory);

    if (!is_null($loadedUser)) {
      $requestService->setApiKey($loadedUser->get('field_api_key')->getString());
    }

    return $requestService;
  }

  /**
   * Creates a `RequestService based on the given input.
   */
  public static function create(ClientFactory $clientFactory,
                                ConfigFactoryInterface $configFactory): HttpRequestService
  {
    $httpFactory    = new HttpFactory();
    $config         = $configFactory->get(CatalogSdk::SETTINGS_KEY);
    $requestService = new HttpRequestService(
      (string) $config->get('api_endpoint'),
      $clientFactory->fromOptions(),
      $httpFactory,
      $httpFactory
    );

    $requestService->setApiKey((string) $config->get('api_key'));

    return $requestService;
  }
}
