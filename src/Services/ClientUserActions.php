<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/catalog_sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\Dexes\catalog_sdk\Services;

use Dexes\CatalogSdk\CatalogSdk;
use Dexes\CatalogSdk\Repository\TokenRepository;
use Dexes\CatalogSdk\Repository\UserRepository;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Dexes\catalog_sdk\CatalogSdk as CatalogSdkConstants;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class ClientUserActions.
 */
final class ClientUserActions
{
  /**
   * Factory for creating a ClientUserActions instance.
   *
   * @param ConfigFactoryInterface        $configFactory The config factory to get the relevant config
   * @param LoggerChannelFactoryInterface $logger        The factory for the appropriate logger
   * @param CatalogSdk                    $catalogSdk    the catalog SDK for interacting with the Dexes Catalog HTTP API
   */
  public static function create(ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $logger,
                                CatalogSdk $catalogSdk): self
  {
    $logger = $logger->get(CatalogSdkConstants::LOGGER_CHANNEL);

    return new self(
      $configFactory->get(CatalogSdkConstants::SETTINGS_KEY)->get('client_user_actions.enabled')         ?? FALSE,
      $configFactory->get(CatalogSdkConstants::SETTINGS_KEY)->get('client_user_actions.username_prefix') ?? '',
      $logger,
      $catalogSdk->users(),
      $catalogSdk->userTokens()
    );
  }

  /**
   * ClientUserActions constructor.
   *
   * @param bool            $enabled              If the client user actions are enabled
   * @param string          $prefix               The prefix for usernames when send to the catalog api
   * @param LoggerInterface $logger               A logger instance
   * @param UserRepository  $userRepository       User-based calls to the Catalog API
   * @param TokenRepository $userTokensRepository User tokens based calls to the catalog api
   */
  public function __construct(private readonly bool $enabled,
                              private readonly string $prefix,
                              private readonly LoggerInterface $logger,
                              private readonly UserRepository $userRepository,
                              private readonly TokenRepository $userTokensRepository)
  {
  }

  /**
   * When creating a user also add it to the catalog api and add an api key.
   *
   * @param UserInterface $user The User that is created
   */
  public function createUser(UserInterface $user): void
  {
    if (!$this->enabled) {
      return;
    }

    $userId = $user->id();

    if (!$user->isActive()) {
      // if !$user->isActive() && hasCredentials then remove token
      // if !$user->isActive() && !hasCredentials then nothing
      try {
        $user->set('field_api_key', '');
        $user->save();
      } catch (EntityStorageException $e) {
        $this->logger->warning('Failed to remove Catalog API credentials for user @user', [
          '@user' => $userId,
        ]);
      }

      return;
    }

    $fieldApiKey = $user->get('field_api_key')->getString();
    if (!empty($fieldApiKey)) {
      // if $user->isActive() && hasCredentials then nothing
      return;
    }

    // if $user->isActive() && !hasCredentials then get credentials
    $email = $user->getEmail();

    if (NULL === $email) {
      $this->logger->warning('No email found for the user, failed to create api user.');

      return;
    }

    $attributes = [
      'name'  => $this->prefix . $user->id(),
      'email' => $email,
    ];

    try {
      $apiUser = $this->userRepository->store($attributes);

      if (!array_key_exists('id', $apiUser)) {
        $this->logger->warning('No id found on user object.');

        return;
      }

      $tokenResponse = $this->userTokensRepository->getToken(strval($apiUser['id']));
    } catch (ClientException|ResponseException $e) {
      $this->logger->error($e->getMessage());

      return;
    }

    try {
      $user->set('field_api_user', $apiUser['id']);
      $user->set('field_api_key', 'Bearer ' . $tokenResponse['token']);

      $user->save();
      $this->logger->info('Generated Catalog API user for @user', [
        '@user' => $userId,
      ]);
    } catch (EntityStorageException $e) {
      $this->logger->warning('Failed to save Catalog API credentials for user @user', [
        '@user' => $userId,
      ]);
    }
  }

  /**
   * When deleting a user also remove it from the Catalog API.
   *
   * @param UserInterface $user The user that is deleted
   */
  public function deleteUser(UserInterface $user): void
  {
    if (!$this->enabled) {
      return;
    }

    $userApiId = $user->get('field_api_user')->getString();

    if (!empty($userApiId)) {
      try {
        $this->userRepository->delete($userApiId);
      } catch (ClientException|ResponseException $e) {
        $this->logger->error($e->getMessage());

        return;
      }
    }
  }
}
