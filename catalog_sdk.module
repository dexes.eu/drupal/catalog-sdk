<?php

/**
 * This file is part of the dexes-drupal/catalog_sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Drupal\Dexes\catalog_sdk\Services\ClientUserActions;
use Drupal\user\UserInterface;

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function catalog_sdk_user_insert(UserInterface $user): void
{
  catalog_sdk_create_api_user($user);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function catalog_sdk_user_update(UserInterface $user): void
{
  catalog_sdk_create_api_user($user);
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function catalog_sdk_user_delete(UserInterface $user): void
{
  /** @var ClientUserActions $apiUserActions */
  $apiUserActions = Drupal::service('catalog_sdk.client_user_actions');
  $apiUserActions->deleteUser($user);
}

function catalog_sdk_create_api_user(UserInterface $user): void
{
  /** @var ClientUserActions $apiUserActions */
  $apiUserActions = Drupal::service('catalog_sdk.client_user_actions');
  $apiUserActions->createUser($user);
}
