<?php

/**
 * This file is part of the dexes-drupal/catalog_sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Dexes\CatalogSdk\HttpRequestService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Http\ClientFactory;
use Drupal\Dexes\catalog_sdk\CatalogSdk;
use Drupal\Dexes\catalog_sdk\HttpRequestServiceFactory;
use GuzzleHttp\Client;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;

/**
 * @internal
 */
class HttpRequestServiceFactoryTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')->with('api_endpoint')->andReturn('https://example.com/');
        $mock->shouldReceive('get')->with('api_key')->andReturn('this is an API key');
      });

      $mock->shouldReceive('get')->with(CatalogSdk::SETTINGS_KEY)->andReturn($config);
    });

    $guzzleFactory = M::mock(ClientFactory::class, function(MI $mock) {
      $mock->shouldReceive('fromOptions')->andReturn(M::mock(Client::class));
    });

    Assert::assertInstanceOf(
      HttpRequestService::class,
      HttpRequestServiceFactory::create($guzzleFactory, $configFactory)
    );
  }
}
